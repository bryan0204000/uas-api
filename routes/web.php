<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteController::class, 'index']);

Route::redirect('/articles', '/');

Route::match(['get', 'post'], '/articles/new', [SiteController::class, 'newArticles']);
Route::match(['get', 'patch'], '/articles/update/{id}', [SiteController::class, 'updateArticles']);
Route::match(['get', 'delete'], '/articles/delete/{id}', [SiteController::class, 'deleteArticles']);

Route::get('/articles/{id}', [SiteController::class, 'getArticles']);

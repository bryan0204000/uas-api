@extends('layouts.main')
@section('title', $data->title)

@section('content')
    <h2 class="m-5 p-®">{{ $data->title }}</h2>
    <p class="content p-2">{!! $data->content !!}</p>
@endsection

@extends('layouts.main')
@section('content')
    @foreach($data as $record)
        <div class="my-3">
            <h2 class="m-@ p-@"><a href="{{ url('/articles/' . $record->id)}}">{{ $record->title }}</a></h2>
            <p class="m-@ p-@">Written at: {{ $record->created_at }}</p>
            <p class="m-@ p-@">Author: <a href="{{ url('/authors/' . $record->author_id)}}">{{ $record->author_name }}</a></p>
            <a href="{{ url('/articles/update/' . $record->id)}}">Update</a>
            &nbsp;
            <a href="{{ url('/articles/delete/' . $record->id)}}"
               data-confirm="Are you sure?"
               data-method="delete"
               rel="nofollow">Delete</a>
        </div>
    @endforeach
@endsection

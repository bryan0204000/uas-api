<?php
namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

class SiteController extends Controller
{
    const API_BASE = 'https://blog-api.stmik-amikbandung.ac.id/api/v2/blog/_table/';
    const API_KEY = 'ef9187e17dce5e8a5da6a5d16ba760b75cadd53d19601a16713e5b7c4F683e1b';
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new Client([
            'base_uri' => self::API_BASE,
            'headers' => [
                'X-DreamFactory-API-Key' => self::API_KEY
            ]
        ]);
    }

    public function index()
    {
        $data = Cache::get('index', function () {
            try {
                $reqData = $this->apiClient->get('articles');
                $resource = json_decode($reqData->getBody())->resource;
                $reqDatas = [];
                foreach($resource as $data){
                    $author = $this->apiClient->get("authors/{$data->author}");
                    $resourceAuthor = json_decode($author->getBody());

                    $reqDatas[] = [
                        "id" => $data->id,
                        "title" => $data->title,
                        "author_id" => $data->author,
                        "author_name" => $resourceAuthor->name,
                        "created_at" => $data->created_at
                    ];
                }
                Cache::add('index', json_decode(json_encode((object) $reqDatas), FALSE));
                return json_decode(json_encode((object) $reqDatas), FALSE);

            } catch (RequestException $e) {
                return [];
            }
        });

        return view('index', ['data' => $data]);
    }

    public function getArticles($id)
    {
        $key = "articles/{$id}";
        $data = Cache::get($key, function () use ($key) {
            try {
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());
                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
                return false;
            }
        });

        return view('viewArticle', ['data' => $data]);
    }

    public function getAuthors($id)
    {
        $key = "authors/{$id}";
        $data = Cache::get($key, function () use ($key) {
            try {
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());
                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
                return false;
            }
        });

        return view('viewArticle', ['data' => $data]);
    }

    public function getComments($id)
    {
        $key = "comments/{$id}";
        $data = Cache::get($key, function () use ($key) {
            try {
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());
                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
                return false;
            }
        });

        return view('viewArticle', ['data' => $data]);
    }

    public function newArticles(Request $request)
    {
        if ($request->isMethod('post')) {
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');
            $dataModel = [
                'resource' => [
                ]
            ];

            $dataModel['resource'][] = [
                'author' => '1',
                'title' => $title,
                'content' => $content
            ];

            try {
                $reqData = $this->apiClient->post('articles', [
                    'json' => $dataModel
                ]);

                $apiResponse = json_decode($reqData->getBody())->resource;
                $newId = $apiResponse[0]->id;

                Cache::forget('index');
                return redirect("/articles/{$newId}");
            } catch (Exception $e) {
                abort(501);
            }
        }
        return view('newArticle');
    }

    public function updateArticles(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');
            $dataModel = [
                'resource' => [
                ]
            ];

            $dataModel['resource'][] = [
                'id' => $id,
                'author' => '1',
                'title' => $title,
                'content' => $content
            ];

            try {
                $reqData = $this->apiClient->patch("articles", [
                    'json' => $dataModel
                ]);

                $apiResponse = json_decode($reqData->getBody())->resource;
                $newId = $apiResponse[0]->id;

                Cache::forget('index');
                return redirect("/articles/{$newId}");
            } catch (Exception $e) {
                abort(501);
            }

            return view('updateArticle');
        }
    }

    public function deleteArticles($id)
    {
        $keyArticle = "articles/{$id}";
        $keyComment = "comments?article={$id}";
        try {
            $deleteComment = $this->apiClient->delete($keyComment, ["article" => $id]);
            $deleteArticle = $this->apiClient->delete($keyArticle, ["id" => $id]);
            $resource = json_decode($deleteArticle->getBody());

            Cache::forget('index');
            return $resource;
        } catch (Exception $e) {
            abort(404);
        }

        return redirect("/articles");
    }
}
